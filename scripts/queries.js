const { keyspaceName, replicationFactor, tables } = require("./constants");

module.exports.queryKeyspace = `
CREATE KEYSPACE IF NOT EXISTS ${keyspaceName}
WITH replication = {'class': 'SimpleStrategy', 'replication_factor': ${replicationFactor}};
`;

module.exports.queryTableFeedbackByProductId = `
CREATE TABLE IF NOT EXISTS ${tables.feedback_by_product_id} (
    product_id TEXT,
    review_id TEXT,
    star_rating TEXT,
    customer_id TEXT,
    review_headline TEXT,
    review_body TEXT,
    review_date TEXT,
    PRIMARY KEY (product_id, review_id)
);
`;

module.exports.queryTableFeedbackByProductIdAndStarRating = `
CREATE TABLE IF NOT EXISTS ${tables.feedback_by_product_id_and_star_rating} (
    product_id TEXT,
    star_rating TEXT,
    review_id TEXT,
    review_headline TEXT,
    review_body TEXT,
    review_date TEXT,
    PRIMARY KEY ((product_id, star_rating))
);
`;

module.exports.queryTableFeedbackByCustomerId = `
CREATE TABLE IF NOT EXISTS ${tables.feedback_by_customer_id} (
    customer_id TEXT,
    star_rating TEXT,
    product_id TEXT,
    review_id TEXT,
    review_headline TEXT,
    review_body TEXT,
    review_date TEXT,
    PRIMARY KEY(customer_id, star_rating)
);
`;

// CASE 4 (firstly get customer_id by product_id and then > N review_count)
module.exports.queryTableCountReviewsCustomerIdByProductId = `
CREATE TABLE IF NOT EXISTS ${tables.count_reviews_customer_id_by_product_id} (
    customer_id TEXT,
    product_id TEXT,
    review_count INT,
    PRIMARY KEY (customer_id, product_id)
);
`;

module.exports.queryInsertFeedbackByProductId = `
INSERT INTO ${tables.feedback_by_product_id} (
    product_id, review_id, star_rating, customer_id, review_headline, review_body, review_date
) VALUES (?, ?, ?, ?, ?, ?, ?);
`;

module.exports.queryInsertFeedbackByProductIdAndStarRating = `
INSERT INTO ${tables.feedback_by_product_id_and_star_rating} (
    product_id, star_rating, review_id, review_headline, review_body, review_date
) VALUES (?, ?, ?, ?, ?, ?);
`;

module.exports.queryInsertFeedbackByByCustomerId = `
INSERT INTO ${tables.feedback_by_customer_id} (
    customer_id, star_rating, product_id, review_id, review_headline, review_body, review_date
) VALUES (?, ?, ?, ?, ?, ?, ?);
`;

// CASE 4 (firstly get customer_id by product_id and then > N review_count)
module.exports.queryInsertCountReviewsCustomerIdByProductId = `
INSERT INTO ${tables.count_reviews_customer_id_by_product_id} (
    customer_id, product_id, review_count
) VALUES (?, ?, 1);
`;

// module.exports.querySelectAllFeedbacksByProductId = `SELECT * FROM ${tables.feedback_by_product_id};`;
// module.exports.querySelectAllFeedbacksByProductIdAndStarRating = `SELECT * FROM ${tables.feedback_by_product_id_and_star_rating};`;
// module.exports.querySelectAllFeedbacksByCustomerId = `SELECT * FROM ${tables.feedback_by_customer_id};`;
module.exports.querySelectAllFeedbacksByProductId = `SELECT review_body FROM ${tables.feedback_by_product_id} WHERE product_id = '1893794008';`;
module.exports.querySelectAllFeedbacksByProductIdAndStarRating = `SELECT review_body FROM ${tables.feedback_by_product_id_and_star_rating} WHERE product_id = '0375826688' AND star_rating = '4';`;
module.exports.querySelectAllFeedbacksByCustomerId = `SELECT review_body FROM ${tables.feedback_by_customer_id} WHERE customer_id = '16234962';`;

// CASE 4 (firstly get customer_id by product_id and then > N review_count)

module.exports.querySelectAllCustomerIdByNwithProductId = `SELECT customer_id FROM ${tables.feedback_by_product_id} WHERE product_id = '0595370640'`;
module.exports.querySelectAllCustomerIdByReviewCount = `SELECT customer_id, review_count FROM ${tables.count_reviews_customer_id_by_product_id} WHERE review_count > 0 ALLOW FILTERING`;

// CASE 5
module.exports.querySelectAllFeedbacksByCustomerIdForStarRating4 = `SELECT star_rating FROM ${tables.feedback_by_customer_id} WHERE customer_id = '16234962' AND star_rating = '4';`;
module.exports.querySelectAllFeedbacksByCustomerIdForStarRating5 = `SELECT star_rating FROM ${tables.feedback_by_customer_id} WHERE customer_id = '16234962' AND star_rating = '5';`;
