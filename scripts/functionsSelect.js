const {
  queryTableCountReviewsCustomerIdByProductId,
  querySelectAllFeedbacksByProductId,
  querySelectAllFeedbacksByProductIdAndStarRating,
  querySelectAllFeedbacksByCustomerId,
  querySelectAllCustomerIdByNwithProductId,
  querySelectAllCustomerIdByReviewCount,
  querySelectAllFeedbacksByCustomerIdForStarRating4,
  querySelectAllFeedbacksByCustomerIdForStarRating5,
} = require("./queries");

const { selectedProductId } = require("./constants");

// CASE 1

module.exports.selectAllFeedbacksByProductId = (client) => {
  client.execute(querySelectAllFeedbacksByProductId, (err, result) => {
    if (err) {
      console.error("Error executing SELECT query:", err);
    } else {
      console.log(
        "querySelectAllFeedbacksByProductId - Selected rows:",
        result.rows
      );
    }
  });
};

// CASE 2

module.exports.selectAllFeedbacksByProductIdAndStarRating = (client) => {
  client.execute(
    querySelectAllFeedbacksByProductIdAndStarRating,
    (err, result) => {
      if (err) {
        console.error("Error executing SELECT query:", err);
      } else {
        console.log(
          "querySelectAllFeedbacksByProductIdAndStarRating - Selected rows:",
          result.rows
        );
      }
    }
  );
};

// CASE 3

module.exports.selectAllFeedbacksByCustomerId = (client) => {
  client.execute(querySelectAllFeedbacksByCustomerId, (err, result) => {
    if (err) {
      console.error("Error executing SELECT query:", err);
    } else {
      console.log(
        "querySelectAllFeedbacksByCustomerId - Selected rows:",
        result.rows
      );
    }
  });
};

// CASE 4 (firstly get customer_id by product_id and then > N review_count)

module.exports.selectAllCustomerIdByNwithProductId = (client) => {
  client.execute(
    querySelectAllCustomerIdByNwithProductId,
    async (err, result) => {
      if (err) {
        console.error("Error executing SELECT query:", err);
      } else {
        console.log(
          "querySelectAllCustomerIdByNwithProductId - Selected rows:",
          result.rows
        );

        if (result.rows.length) {
          await client.execute(queryTableCountReviewsCustomerIdByProductId);

          result.rows.map(async (row) => {
            let ueryInsertCountReviewsCustomerIdByProductId = `
            INSERT INTO ${tables.count_reviews_customer_id_by_product_id} (customer_id, product_id, review_count) VALUES (?, ?, ${result.rows.length});`;
            let values = [row.customer_id, selectedProductId];

            console.log("values", values);
            client.execute(
              ueryInsertCountReviewsCustomerIdByProductId,
              values,
              (err, result) => {
                if (err) {
                  console.error(
                    "ERROR queryInsertCountReviewsCustomerIdByProductId QUERY:",
                    err
                  );
                } else {
                  console.log(
                    "queryInsertCountReviewsCustomerIdByProductId - Data inserted successfully",
                    result
                  );
                }
              }
            );
          });
        }
      }
    }
  );
};

module.exports.selectAllCustomerIdByReviewCount = (client) => {
  client.execute(querySelectAllCustomerIdByReviewCount, (err, result) => {
    if (err) {
      console.error(
        "Error executing SELECT querySelectAllCustomerIdByReviewCount:",
        err
      );
    } else {
      console.log(
        "querySelectAllCustomerIdByReviewCount - Selected rows:",
        result.rows
      );
    }
  });
};

// CASE 5

let sumStarRating = 0;

module.exports.selectAllFeedbacksByCustomerIdForStarRating4 = (client) => {
  client.execute(
    querySelectAllFeedbacksByCustomerIdForStarRating4,
    (err, result) => {
      if (err) {
        console.error("Error executing SELECT query:", err);
      } else {
        sumStarRating += result.rows.length;
        console.log(
          "querySelectAllFeedbacksByCustomerIdForStarRating4 - Selected rows:",
          result.rows
        );
        console.log("sumStarRating", sumStarRating);
      }
    }
  );
};

module.exports.selectAllFeedbacksByCustomerIdForStarRating5 = (client) => {
  client.execute(
    querySelectAllFeedbacksByCustomerIdForStarRating5,
    (err, result) => {
      if (err) {
        console.error("Error executing SELECT query:", err);
      } else {
        sumStarRating += result.rows.length;
        console.log(
          "querySelectAllFeedbacksByCustomerIdForStarRating5 - Selected rows:",
          result.rows
        );
        console.log("sumStarRating", sumStarRating);
      }
    }
  );
};
