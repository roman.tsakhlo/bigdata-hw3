const pathToFile = "./amazon_reviews.csv";

module.exports.keyspaceName = "amazontable";
module.exports.replicationFactor = "3";

module.exports.tables = {
  feedback_by_product_id: "feedback_by_product_id",
  feedback_by_product_id_and_star_rating:
    "feedback_by_product_id_and_star_rating",
  feedback_by_customer_id: "feedback_by_customer_id",
  count_reviews_customer_id_by_product_id:
    "count_reviews_customer_id_by_product_id",
};

module.exports.selectedProductId = "0595370640";
