const {
  queryInsertFeedbackByProductId,
  queryInsertFeedbackByProductIdAndStarRating,
  queryInsertFeedbackByByCustomerId,
} = require("./queries");

module.exports.insertFeedbackByProductId = (client, values) => {
  client.execute(queryInsertFeedbackByProductId, values, (err, result) => {
    if (err) {
      console.error("ERROR queryInsertFeedbackByProductId QUERY:", err);
    } else {
      console.log(
        "queryInsertFeedbackByProductId - Data inserted successfully",
        result
      );
    }
  });
};

module.exports.insertFeedbackByProductIdAndStarRating = (client, values) => {
  client.execute(
    queryInsertFeedbackByProductIdAndStarRating,
    values,
    (err, result) => {
      if (err) {
        console.error(
          "ERROR queryInsertFeedbackByProductIdAndStarRating QUERY:",
          err
        );
      } else {
        console.log(
          "queryInsertFeedbackByProductIdAndStarRating - Data inserted successfully",
          result
        );
      }
    }
  );
};

module.exports.insertFeedbackByByCustomerId = (client, values) => {
  client.execute(queryInsertFeedbackByByCustomerId, values, (err, result) => {
    if (err) {
      console.error("ERROR queryInsertFeedbackByByCustomerId QUERY:", err);
    } else {
      console.log(
        "queryInsertFeedbackByByCustomerId - Data inserted successfully",
        result
      );
    }
  });
};
