const fs = require("fs");
const { Client } = require("cassandra-driver");
const { keyspaceName } = require("./scripts/constants");
const {
  queryKeyspace,
  queryTableFeedbackByProductId,
  queryTableFeedbackByProductIdAndStarRating,
  queryTableFeedbackByCustomerId,
} = require("./scripts/queries");

const {
  insertFeedbackByProductId,
  insertFeedbackByProductIdAndStarRating,
  insertFeedbackByByCustomerId,
} = require("./scripts/functionsInsert");

const {
  selectAllFeedbacksByProductId,
  selectAllFeedbacksByProductIdAndStarRating,
  selectAllFeedbacksByCustomerId,
  selectAllCustomerIdByNwithProductId,
  selectAllCustomerIdByReviewCount,
  selectAllFeedbacksByCustomerIdForStarRating4,
  selectAllFeedbacksByCustomerIdForStarRating5,
} = require("./scripts/functionsSelect");

const client = new Client({
  contactPoints: ["127.0.0.1"],
  localDataCenter: "datacenter1",
  keyspace: keyspaceName,
});

async function createKeySpace() {
  const initialClient = new Client({
    contactPoints: ["127.0.0.1"],
    localDataCenter: "datacenter1",
    keyspace: "system",
  });

  await initialClient.connect();
  await initialClient.execute(queryKeyspace, (err) => {
    if (err) {
      console.error("ERROR CREATING KEYSPACE:", err);
    } else {
      console.log(`Keyspace '${keyspaceName}' created or already exists`);
    }
  });
}

async function createTables() {
  await client.execute(queryTableFeedbackByProductId);
  await client.execute(queryTableFeedbackByProductIdAndStarRating);
  await client.execute(queryTableFeedbackByCustomerId);
  console.log("Tables created successfully");
}

async function insertData() {
  const pathToFile = "./amazon_reviews.csv";
  const fileData = fs.readFileSync(pathToFile, "utf-8");
  const lines = fileData.split("\n");
  const headers = lines[0].split(",");

  for (let i = 1; i < lines.length / 1000; i++) {
    const allValues = lines[i].split(",");
    const fields = {};

    for (let j = 0; j < headers.length; j++) {
      fields[headers[j]] = allValues[j];
    }

    let valuesProductId = [
      fields.product_id,
      fields.review_id,
      fields.star_rating,
      fields.customer_id,
      fields.review_headline,
      fields.review_body,
      fields.review_date,
    ];

    let valuesProductIdAndStarRating = [
      fields.product_id,
      fields.star_rating,
      fields.review_id,
      fields.review_headline,
      fields.review_body,
      fields.review_date,
    ];

    let valuesCustomerId = [
      fields.customer_id,
      fields.star_rating,
      fields.product_id,
      fields.review_id,
      fields.review_headline,
      fields.review_body,
      fields.review_date,
    ];

    insertFeedbackByProductId(client, valuesProductId);
    insertFeedbackByProductIdAndStarRating(
      client,
      valuesProductIdAndStarRating
    );
    insertFeedbackByByCustomerId(client, valuesCustomerId);
  }

  console.log("Data loaded successfully");
}

async function selectData() {
  // CASE 1
  // selectAllFeedbacksByProductId(client);
  // CASE 2
  // selectAllFeedbacksByProductIdAndStarRating(client);
  // CASE 3
  // selectAllFeedbacksByCustomerId(client);
  // CASE 4 (firstly get customer_id by product_id and then > N review_count)
  // selectAllCustomerIdByNwithProductId(client);
  // selectAllCustomerIdByReviewCount(client);
  // CASE 5
  // selectAllFeedbacksByCustomerIdForStarRating4(client);
  // selectAllFeedbacksByCustomerIdForStarRating5(client);
}

async function run() {
  try {
    await client.connect();
    await createKeySpace();
    await createTables();
    // await insertData();
    // await selectData();
  } catch (error) {
    console.error("RUN ERROR", error);
  } finally {
    // await client.shutdown();
  }
}

run();
